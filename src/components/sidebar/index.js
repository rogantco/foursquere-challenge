import React, { Component } from 'react';
import { MenuList, MenuItem } from 'material-ui/Menu';
import { ListItemIcon, ListItemText } from 'material-ui/List';
import PropTypes from 'prop-types';
import { withStyles } from "material-ui/styles/index";

import RestaurantIcon from '@material-ui/icons/Restaurant';
import CoffeeIcon from '@material-ui/icons/LocalCafe';
import NightlifeIcon from '@material-ui/icons/LocalDrink';
import ArtsIcon from '@material-ui/icons/LocalPlay';
import ShoppingIcon from '@material-ui/icons/LocalMall';

const styles = theme => ({
    toolbar: theme.mixins.toolbar,
});

class Sidebar extends Component {
    state = {
        filters: [
            {
                active: false,
                icon: RestaurantIcon,
                label: 'Restaurants',
                key: 'food'
            },
            {
                active: false,
                icon: CoffeeIcon,
                label: 'Coffee',
                key: 'coffee'
            },
            {
                active: false,
                icon: NightlifeIcon,
                label: 'Nightlife',
                key: 'drinks'
            },
            {
                active: false,
                icon: ArtsIcon,
                label: 'Arts',
                key: 'arts'
            },
            {
                active: false,
                icon: ShoppingIcon,
                label: 'Shopping',
                key: 'shops'
            },
        ]
    };

    renderFilters() {
        return this.state.filters.map((filter) => {
            const Icon = filter.icon;
            return (
                <MenuItem key={filter.key} button onClick={() => { this.filterHandler(filter) }} selected={filter.active}>
                    <ListItemIcon>
                        <Icon />
                    </ListItemIcon>
                    <ListItemText primary={filter.label} />
                </MenuItem>
            );
        })
    }

    filterHandler(filter) {
        const activeFilter = this.state.filters.find(filter => filter.active) || false;

        if (activeFilter) {
            activeFilter.active = false;
        }

        if (filter.key !== activeFilter.key) {
            filter.active = true;
            this.props.onSidebarFilterChange(filter.key)
        } else {
            this.props.onSidebarFilterChange('')
        }

        this.setState(Object.assign({}, this.state.filters, [filter, activeFilter]));
    }

    render() {
        const { classes } = this.props;

        return (
            <div>
                <div className={classes.toolbar} />
                <MenuList>
                    {this.renderFilters()}
                </MenuList>
            </div>
        );
    }
}

Sidebar.propTypes = {
    classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(Sidebar);
