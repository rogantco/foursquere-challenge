import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import GridList, { GridListTile, GridListTileBar } from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import InfoIcon from '@material-ui/icons/Info';

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
    },
    gridList: {
        width: '100%',
        height: '100%',
    },
    icon: {
        color: 'rgba(255, 255, 255, 0.54)',
    },
});

const VenuesGridList = (props) => {
    const { classes, venues } = props;

    return (
        <div className={classes.root}>
            <GridList cellHeight={300} cols={3} className={classes.gridList}>
                {venues.map(({ venue }) => (
                    <GridListTile key={venue.id}>
                        <img src={venue.imageURL} alt={venue.name} />
                        <GridListTileBar
                            title={venue.name}
                            subtitle={<span>{venue.location.address}</span>}
                            actionIcon={
                                <a href={`https://foursquare.com/v/${venue.id}`} target="_blank">
                                    <IconButton className={classes.icon}>
                                        <InfoIcon />
                                    </IconButton>
                                </a>
                            }
                        />
                    </GridListTile>
                ))}
            </GridList>
        </div>
    );
};

VenuesGridList.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(VenuesGridList);