import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Drawer from 'material-ui/Drawer';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import { withStyles } from "material-ui/styles/index";

import Sidebar from './../../components/sidebar';
import VenuesGridList from './../../components/gridList';

const drawerWidth = 240;

const styles = theme => ({
    root: {
        flexGrow: 1,
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
    },
    drawerPaper: {
        position: 'relative',
        width: drawerWidth,
    },
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
    },
    loader: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        textAlign: 'center',
        height: '100%'
    },
    toolbar: theme.mixins.toolbar,
});

class Home extends Component {
    state = {
        position: {
            latitude: parseFloat(localStorage.latitude) || '',
            longitude: parseFloat(localStorage.longitude) || '',
        },
        venues: [],
        filter: '',
    };

    componentDidMount() {
        if (this.state.position.latitude && this.state.position.longitude) {
            this.fetchVenues();
        }

        if ('geolocation' in navigator) {
            navigator.geolocation.getCurrentPosition((position) => {
                this.setState({ position: position.coords }, () => {
                    this.fetchVenues();
                });
                localStorage.setItem('latitude', position.coords.latitude);
                localStorage.setItem('longitude', position.coords.longitude);
            }, () => {
                alert('Please eneble the Location services at your System Preferences');
            }, {
                timeout: 3000
            });
        }
    }

    fetchVenues() {
        const params = {
            client_id: 'RR1OYBBIV1IQ2C1TVD4DTFCP5FKYIPTEVFMOPLVXCASXJZKF',
            client_secret: 'ILG0Y3UUW2UU3YESHVWSZIAVODTINGFOSNVAIGFWBF4C4T1P',
            ll: `${this.state.position.latitude},${this.state.position.longitude}`,
            v: '20180323',
            section: this.state.filter,
            limit: 12
        };
        const localStorageKey = JSON.stringify(params);
        const cachedVenues = localStorage.getItem(localStorageKey);

        if (!cachedVenues) {
            fetch('https://api.foursquare.com/v2/venues/explore/?' + new URLSearchParams(params))
                .then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson.meta.code === 200) {
                        const venues = responseJson.response.groups[0].items;

                        venues.forEach((venue) => {
                            fetch(`https://api.foursquare.com/v2/venues/${venue.venue.id}/photos/?` + new URLSearchParams(params))
                                .then((response) => response.json())
                                .then((responsePhotos) => {
                                    const rawImage = responsePhotos.response.photos.items[0];
                                    const defaultImage = './no-thumbnail.png';
                                    let imageVenue = venue;

                                    imageVenue.venue.imageURL = rawImage ? `${rawImage.prefix}500x300${rawImage.suffix}` : defaultImage;
                                    this.setState(Object.assign({}, this.state.imageVenue, imageVenue));
                                    localStorage.setItem(localStorageKey, JSON.stringify(venues))
                                })

                        });

                        this.setState({ venues: venues })
                    }
                });
        } else {
            const venues = JSON.parse(cachedVenues);
            this.setState({ venues: venues })
        }
    }

    onSidebarFilterChange = (filter) => {
        this.setState({ filter }, () => {
            this.fetchVenues();
        });
    };

    renderGrid() {
        if (this.state.venues.length > 0) {
            return (
                <VenuesGridList venues={this.state.venues} />
            )
        } else {
            return (
                <Typography className={this.props.classes.loader}>Loading...</Typography>
            )
        }
    }

    render() {
        const { classes } = this.props;

        return (
            <div className={classes.root}>
                <AppBar position="absolute" className={classes.appBar}>
                    <Toolbar>
                        <Typography variant="title" color="inherit" noWrap>
                            Foursquare Client
                        </Typography>
                    </Toolbar>
                </AppBar>
                <Drawer
                    variant="permanent"
                    classes={{
                        paper: classes.drawerPaper,
                    }}
                >
                    <Sidebar onSidebarFilterChange={this.onSidebarFilterChange} />
                </Drawer>
                <main className={classes.content}>
                    <div className={classes.toolbar} />
                    {this.renderGrid()}
                </main>
            </div>
        )
    }
}

Home.propTypes = {
    classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(Home);
